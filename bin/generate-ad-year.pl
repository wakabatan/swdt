use strict;
use warnings;
use utf8;
use Path::Tiny;

my $RootPath = path (__FILE__)->parent->parent;
my $DataPath = $RootPath->child ('year/ad');

sub html_year ($) {
  my $year = shift;
  if ($year <= 0) {
    return sprintf q{<a href="%d.html">西暦%d年 (紀元前%d年)</a> }, $year, $year, 1-$year;
  } else {
    return sprintf q{<a href="%d.html">西暦%d年</a>}, $year, $year;
  }
} # html_year

sub is_leap_year ($$) {
  my ($cal, $year) = @_;
  if ($cal eq 'gregorian') {
    return 0 if $year % 4;
    return 1 if ($year % 400) == 0;
    return 0 if ($year % 100) == 0;
    return 1;
  } elsif ($cal eq 'julian') {
    return 0 if $year % 4;
    return 1;
  } else {
    die $cal;
  }
} # is_leap_year

sub next_leap_year ($$$) {
  my ($cal, $year, $dir) = @_;
  for my $delta (1..10) {
    my $y = $year + $delta * $dir;
    return $y if is_leap_year $cal, $y;
  }
  die;
} # next_leap_year

sub generate_year ($) {
  my $year = shift;
  my $path = $DataPath->child ("$year.html");
  my $html = qq{<!DOCTYPE HTML>
    <html lang=ja>
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel=stylesheet href=https://wiki.suikawiki.org/styles/sw>
    <title>西暦@{[$year]}年</title>
    <link rel=prev href="@{[$year-1]}.html">
    <link rel=next href="@{[$year+1]}.html">

    <h1>西暦@{[$year]}年</h1>

    <p>西暦@{[$year]}年は、西暦元年から@{[$year-1]}年後の年です。
    前の年は@{[html_year $year-1]}、
    次の年は@{[html_year $year+1]}です。

    @{[
      $year <= 0 ? qq{西暦@{[$year]}年は、西暦紀元前@{[1-$year]}年です。} : ''
    ]}

    <section id=gregorian class="section">
      <h1>グレゴリオ暦西暦@{[$year]}年</h1>

      <p>グレゴリオ暦の西暦@{[$year]}年は、
      @{[
        (is_leap_year 'gregorian', $year)
            ? q{閏年です。366日あります。}
            : q{平年です。365日あります。}
      ]}
      前の閏年は@{[
        html_year next_leap_year 'gregorian', $year, -1
      ]}、
      次の閏年は@{[
        html_year next_leap_year 'gregorian', $year, +1
      ]}です。
    </section>

    <section id=julian class=section>
      <h1>ユリウス暦西暦@{[$year]}年</h1>

      <p>ユリウス暦の西暦@{[$year]}年は、
      @{[
        (is_leap_year 'julian', $year)
            ? q{閏年です。366日あります。}
            : q{平年です。365日あります。}
      ]}
      前の閏年は@{[
        html_year next_leap_year 'julian', $year, -1
      ]}、
      次の閏年は@{[
        html_year next_leap_year 'julian', $year, +1
      ]}です。
    </section>

    <footer class=footer>
      <a href=../../ rel=top>トップページ</a>
    </footer>
  };
  $path->spew_utf8 ($html);
} # generate_year

for (-4712..3000) {
  generate_year $_;
}
